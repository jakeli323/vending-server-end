<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    // +----------------------------------------------------------------------
    // | 应用设置
    // +----------------------------------------------------------------------

    // 应用调试模式
    'app_debug'              => false,
    // 应用Trace
    'app_trace'              => false,
    // 应用模式状态
    'app_status'             => '',
    // 是否支持多模块
    'app_multi_module'       => true,
    // 入口自动绑定模块
    'auto_bind_module'       => false,
    // 注册的根命名空间
    'root_namespace'         => [],
    // 扩展函数文件
    'extra_file_list'        => [THINK_PATH . 'helper' . EXT],
    // 默认输出类型
    'default_return_type'    => 'html',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return'    => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler'  => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler'      => 'callback',
    // 默认时区
    'default_timezone'       => 'PRC',
    // 是否开启多语言
    'lang_switch_on'         => false,
    // 默认全局过滤方法 用逗号分隔多个
    'default_filter'         => '',
    // 默认语言
    'default_lang'           => 'zh-cn',
    // 应用类库后缀
    'class_suffix'           => false,
    // 控制器类后缀
    'controller_suffix'      => false,

    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------

    // 默认模块名
    'default_module'         => 'index',
    // 禁止访问模块
    'deny_module_list'       => ['common'],
    // 默认控制器名
    'default_controller'     => 'Index',
    // 默认操作名
    'default_action'         => 'index',
    // 默认验证器
    'default_validate'       => '',
    // 默认的空控制器名
    'empty_controller'       => 'Error',
    // 操作方法后缀
    'action_suffix'          => '',
    // 自动搜索控制器
    'controller_auto_search' => false,

    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------

    // PATHINFO变量名 用于兼容模式
    'var_pathinfo'           => 's',
    // 兼容PATH_INFO获取
    'pathinfo_fetch'         => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],
    // pathinfo分隔符
    'pathinfo_depr'          => '/',
    // URL伪静态后缀
    'url_html_suffix'        => 'html',
    // URL普通方式参数 用于自动生成
    'url_common_param'       => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type'         => 0,
    // 是否开启路由
    'url_route_on'           => true,
    // 路由使用完整匹配
    'route_complete_match'   => false,
    // 路由配置文件（支持配置多个）
    'route_config_file'      => ['route'],
    // 是否强制使用路由
    'url_route_must'         => false,
    // 域名部署
    'url_domain_deploy'      => false,
    // 域名根，如thinkphp.cn
    'url_domain_root'        => '',
    // 是否自动转换URL中的控制器和操作名
    'url_convert'            => true,
    // 默认的访问控制器层
    'url_controller_layer'   => 'controller',
    // 表单请求类型伪装变量
    'var_method'             => '_method',
    // 表单ajax伪装变量
    'var_ajax'               => '_ajax',
    // 表单pjax伪装变量
    'var_pjax'               => '_pjax',
    // 是否开启请求缓存 true自动缓存 支持设置请求缓存规则
    'request_cache'          => false,
    // 请求缓存有效期
    'request_cache_expire'   => null,
    // 全局请求缓存排除规则
    'request_cache_except'   => [],

    // +----------------------------------------------------------------------
    // | 模板设置
    // +----------------------------------------------------------------------

    'template'               => [
        // 模板引擎类型 支持 php think 支持扩展
        'type'         => 'Think',
        // 模板路径
        'view_path'    => '',
        // 模板后缀
        'view_suffix'  => 'html',
        // 模板文件名分隔符
        'view_depr'    => DS,
        // 模板引擎普通标签开始标记
        'tpl_begin'    => '{',
        // 模板引擎普通标签结束标记
        'tpl_end'      => '}',
        // 标签库标签开始标记
        'taglib_begin' => '{',
        // 标签库标签结束标记
        'taglib_end'   => '}',
        // 'layout_on'  => true,
        // 'layout_name'  => 'layout/mainlayout',
        // 'layout_item'  =>  '{__CONTENT__}',
    ],

    // 视图输出字符串内容替换
    'view_replace_str'       => [
        '__PUBLIC__' =>  '/public/static',
        '__JS__'  =>  '/public/static/js',
        '__CSS__' =>  '/public/static/css',
        '__IMAGE__' => '/public/static/img',
        '__EE__' => 'hellowlrld！！！',

    ],

    'upload_paths' => [
        'itemtypes' => '/public/uploads/itemtypes/img',
		'imgtypes' => '/public/uploads/imgtypes/img',
		'apk' => '/public/uploads/apk',
     ],


    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => THINK_PATH . 'tpl' . DS . 'dispatch_jump.tpl',
    'dispatch_error_tmpl'    => THINK_PATH . 'tpl' . DS . 'dispatch_jump.tpl',

    // +----------------------------------------------------------------------
    // | 异常及错误设置
    // +----------------------------------------------------------------------

    // 异常页面的模板文件
    'exception_tmpl'         => THINK_PATH . 'tpl' . DS . 'think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'          => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'         => false,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle'       => '',

    // +----------------------------------------------------------------------
    // | 日志设置
    // +----------------------------------------------------------------------

    'log'                    => [
        // 日志记录方式，内置 file socket 支持扩展
        'type'  => 'File',
        // 日志保存目录
        'path'  => LOG_PATH,
        // 日志记录级别
        'level' => [],
    ],

    // +----------------------------------------------------------------------
    // | Trace设置 开启 app_trace 后 有效
    // +----------------------------------------------------------------------
    'trace'                  => [
        // 内置Html Console 支持扩展
        'type' => 'Html',
    ],

    // +----------------------------------------------------------------------
    // | 缓存设置
    // +----------------------------------------------------------------------

    'cache'                  => [
        // 驱动方式
        'type'   => 'File',
        // 缓存保存目录
        'path'   => CACHE_PATH,
        // 缓存前缀
        'prefix' => '',
        // 缓存有效期 0表示永久缓存
        'expire' => 0,
    ],

    // +----------------------------------------------------------------------
    // | 会话设置
    // +----------------------------------------------------------------------

    'session'                => [
        'id'             => '',
        // SESSION_ID的提交变量,解决flash上传跨域
        'var_session_id' => '',
        // SESSION 前缀
        'prefix'         => 'think',
        // 驱动方式 支持redis memcache memcached
        'type'           => '',
        // 是否自动开启 SESSION
        'auto_start'     => true,
    ],

    // +----------------------------------------------------------------------
    // | Cookie设置
    // +----------------------------------------------------------------------
    'cookie'                 => [
        // cookie 名称前缀
        'prefix'    => '',
        // cookie 保存时间
        'expire'    => 0,
        // cookie 保存路径
        'path'      => '/',
        // cookie 有效域名
        'domain'    => '',
        //  cookie 启用安全传输
        'secure'    => false,
        // httponly设置
        'httponly'  => '',
        // 是否使用 setcookie
        'setcookie' => true,
    ],
	
'alipay' =>[
        'app_id' => "2016091800536294",
        //商户私钥
        'merchant_private_key' => "MIIEowIBAAKCAQEAwsbt9gwvPguYatLmlmP5bGypDXLggsXvaubVfDfBeaTOLS8FG9OFart/0uUaKpJJQUxKiB7r99W0lz27ySRnQY0zUdUHqp7bOEvtmkSOaGmywHt2kh+vCqM+PySCQR+OzfSbECMoAZSKjNR5lJbW9HxICn76u/dNIPsTyuEnpWx2PeD0TyULAVvewXKyZAq8R2asrPsyNR/sxgifbZ7HspgD2IhdiTu9sos5//DZZTd4QZvyft0yDwTEsa8QUldWzIQ30SvIofx6/eoEOwW1MYW6mwbYESxltHdjyonzL33muQBNHqJWf0aQBCcFEpktysOQAdH/Mb7w8V2RhnDXpQIDAQABAoIBAQCFMmaumPyBkIplmt4HQEzkYZZl1QZFR7iHQV4CuVo8poA8E+iJc+fL6nbqMLPpIuqnldVuKr8xV1pNBvFxBsz53eaXcnfwrPu7DuloUrOUIZwU1xlpqfMY8jNyGLRedodAvqJ/sZr9Iahx0CdofpJ21LqrOtSaT+7Vu/14AusKyuONZqDB/nHrpa1JEE7H7KQulnos7Y3IUaBAyT05nOn1YQcvdv3JgVNQLnbpFYv7JYdqVL5FnXhe+p1tvoAcJegJ5oIESsbmwVQf5a/+CbIDFl9S3yVg9jy8nq+g0rHPq9OcO1q0hPSSYFvqFtd0+wqYrNL+A8KR+4EjA85VI6khAoGBAPSvJxUm2PBkDZoFucOfp5vA7Y8LxwD4/CM/8HwqjRGRb0XTskzXLCgb9EiTFTVF5QxWYHbJTugyBB0ztOHPvNaQfqqBHntTBYiprKZtqOynO4SQsmUeyYXAo3mHGHpj0zWU3cmypFMHaD753akk41noBufnebgnjiC4s5N3wIpZAoGBAMvI64sbEpPkxbh/HoAwzQGasin9CI8hpn6Mxk/EFREgqwD/ElgAt1JcA1vJlfy2nazt5iV+gP8bMjjBQr5ZWbrqiQLbmaXKte0eSjJLaGbKOqkNbcrQ0vLDZeX1ldH1q4ewHzTXSO7kK6scrN3lu7mZovWxCCxR6tVh020W//YtAoGAIskH0Fz+eGZw/JWVzDbVWDI8U5rEBuJezhAX+z79YSMA47dt4LiyYV1gBDVMzY8LOg7JlfmWizIZHaGtNHEZEpPmPg0vLS8nWT9t+Rcb7dD0iCseAXw4ABjb5LoNA9FPRI3OXg9Uhy1FZ7aUCajh3XU8Dhn9Vu3gqcw4U12x9LkCgYAUXr4kxo0B2ZFmEtbiq34ilesPRWrQm+V/quS73nbYonJVhCm6QEAS7H2tdCYY/8wdGB60SsPsPDS19aONUXMluwH9FiRyrSOoUynJgz4pYDei5GPALUWY3K1X4keamDsQMiu8ZXS15Hr0/MXO3IL6V4AxnQObIokd0UyIEA9/9QKBgHjBHHeBHu31kpPdhq/raf3cRPGb0Z81th/531JBLSGQSWfNebDb5mT7+OuRyGu55QEfTdSCUnXyK3DF+8BgCP26g/aRPiVoGH+14esWqqVHxbGRvsnr8FR41CB4mqMWfazhk8pbySwJT05Ay1b+PvIjbkJL8+vL3dWF/49pJHrw",
        //编码格式
        'charset' => "UTF-8",
		
	     //异步通知地址
		'notify_url' => "http://47.106.112.193/public/index.php/index/login/index.html",
		
	    //同步跳转
		'return_url' => "www.baidu.com",
        //签名方式
        'sign_type'=>"RSA2",
        //支付宝网关
        'gatewayUrl' => "https://openapi.alipay.com/gateway.do",
        //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
        'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwsbt9gwvPguYatLmlmP5bGypDXLggsXvaubVfDfBeaTOLS8FG9OFart/0uUaKpJJQUxKiB7r99W0lz27ySRnQY0zUdUHqp7bOEvtmkSOaGmywHt2kh+vCqM+PySCQR+OzfSbECMoAZSKjNR5lJbW9HxICn76u/dNIPsTyuEnpWx2PeD0TyULAVvewXKyZAq8R2asrPsyNR/sxgifbZ7HspgD2IhdiTu9sos5//DZZTd4QZvyft0yDwTEsa8QUldWzIQ30SvIofx6/eoEOwW1MYW6mwbYESxltHdjyonzL33muQBNHqJWf0aQBCcFEpktysOQAdH/Mb7w8V2RhnDXpQIDAQAB",
    ],	
	
	
	
	

    //分页配置
    'paginate'               => [
        'type'      => 'bootstrap',
        'var_page'  => 'page',
        'list_rows' => 15,
    ],
];
