<?php
namespace app\user\controller;

use app\index\model\User;
use think\View;
use think\Db;
use think\Controller;

class Members extends Controller{
	public function index(){
		echo "</br>1 1 1 11 1 1 1 1 1 1 1 1 1 1 </br>";
		// print_r(Db::table('think_user')->where('user_id', 7)->column('user_name'));
		// print_r(Db::table('think_user')->field('user_id, user_name')->select());		

		echo "</br>2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 </br>";
		//查询出用户列表
		$list = User::all(function($query){
			$query->where('status',1)->limit(10)->order('user_id','asc');
		});

		// print_r($list);

		echo "</br>2 3 3 3 3 3 3 3 3 3 3 3 </br>";

		
		// $view = new View;
		//设置变量输出
		$this->view->assign('list',$list);

		return $this->view->fetch('index');
	}
}