<?php
namespace app\user\controller;
use app\user\model;
use think\Db;

class Goods extends Base
{
    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Goods');
    }
    
    //信息列表
    public function index()
    {
        $vendorUid = input("param.vendor_uid");
        // $list = Db::name('goods')->select();

        $list = Db::table("think_goods")->alias('g')->join('item_type i', 'g.item_type_uid=i.uid')->field('g.*, i.img, i.name, i.file_name')->where('g.vendor_uid', $vendorUid)->select();
        trace(json_encode($list, JSON_UNESCAPED_SLASHES),"info");
        //to be continue 这里的uid指的是item type uid的uid，我希望两个都看到，但是两个字段是一样的
        ///最后只是显示其中一个。
//trace ($list,"info");
//foreach($list as $key=>$value){
//$list[$key]['img']= 'http://47.106.112.193'.$value['img'];
//} 

 trace($list,"info");
        $slot=array();
        foreach($list as $list2){
        $slot[]=$list2["slot"];
        }
		trace($slot,"info");
        array_multisort($slot, SORT_ASC, $list);
        $this->assign('list', $list);

        $this->assign("vendorUid", $vendorUid);


        return $this->fetch('goods/index');
    }
	
	public function showJson(){
		header("Access-Control-Allow-Origin:* ");
		$goods = $_POST["goods"];
		trace("1111","info");
		
		
		$addlist2 = Db::name('goods')->where(['uid'=>"5b18df232db74"])->select();
		trace($addlist2[0]["remain_count"],"info");
		trace($goods["number"],"info");
		$result= $addlist2[0]["remain_count"]-$goods["number"];
		trace($result,"info");
		//$addlist2[0]["remain_count"]=$result;
		//$result1 = $this->model->where(['uid'=>"5b18df232db74"])->update($addlist2);
		
		//$user = User::get(1);
		//trace($user,"info");		
        //$user->name     = 'thinkphp';
        //$user->email    = 'thinkphp@qq.com'; 
        //$user->save();
		
		
		
		
		
		//echo $addlist2["remain_count"];
		//$remain_count= $addlist2["remain_count"]-$goods["number"];
		//echo $remain_count;
	}

    public function saveInfo(){
        // item_type_uid
        $data = input("param.");
        $itemTypeUid = input("param.item_type_uid");
        $vendorUid = input("param.vendor_uid");
        $slot = input("param.slot");
        $price = input("param.price");
        $uid = input("param.uid");
		
		$status =input("param.status");
		
        $list = Db::table("think_goods")->alias('g')->join('item_type i', 'g.item_type_uid=i.uid')->field('g.*, i.img, i.name, i.file_name')->where('g.vendor_uid', $vendorUid)->select();

        $slot1=array();
        foreach($list as $list2){
        $slot1[]=$list2["slot"];
        }
		trace($slot1,"info");

        if(in_array($slot,$slot1)){
        trace("已存在","info");
        return $this->error("修改失败，该插槽已占用","user/goods/editinfo?&vendor_uid={$vendorUid}");
        }else{
        trace("不存在","info");
        }		
		
        if(empty($vendorUid)){
            return $this->error("修改失败", "user/vending/index");
        }
		

        $cond = "uid='{$uid}'";
        $queryResult = $this->model->where($cond)->find();


        $param = array();
        $param['uid'] = $uid;
        $param['vendor_uid'] = $vendorUid;

        // 如果数据库不存在vendoruid和itemtypeuid，那么添加
        if($queryResult){
			
            $result = $this->model->where($cond)->update($data);

            if($result){
                return $this->success('修改成功', "user/goods/index?&vendor_uid={$vendorUid}");
            }else{
                return $this->error('修改失败', "user/vending/index");    
            }
            
        }else{

            //todo 这里要进行一次参数validate

            //这个目前先写死
            $data['remain_count'] = 0;
			$data['status'] = 1;
            $data['uid'] = uniqid();

            $result = $this->model->data($data)->save();
            if($result){
                // return $this->success('修改成功', "user/goods/index");
                return $this->success('修改成功', "user/goods/index?&vendor_uid={$vendorUid}");
            }else{
                return $this->error('修改失败', "user/vending/index");    
            }
        }
    }

    public function saveInfo_ex(){
        // item_type_uid
        $data = input("param.");
        $itemTypeUid = input("param.item_type_uid");
        $vendorUid = input("param.vendor_uid");
        $slot = input("param.slot");
        $price = input("param.price");
        $uid = input("param.uid");
		
		$status =input("param.status");
		
        $list = Db::table("think_goods")->alias('g')->join('item_type i', 'g.item_type_uid=i.uid')->field('g.*, i.img, i.name, i.file_name')->where('g.vendor_uid', $vendorUid)->select();

        $slot1=array();
        foreach($list as $list2){
        $slot1[]=$list2["slot"];
        }
		trace($slot1,"info");
				
        if(empty($vendorUid)){
            return $this->error("修改失败", "user/vending/index");
        }
		

        $cond = "uid='{$uid}'";
        $queryResult = $this->model->where($cond)->find();


        $param = array();
        $param['uid'] = $uid;
        $param['vendor_uid'] = $vendorUid;

        // 如果数据库不存在vendoruid和itemtypeuid，那么添加
        if($queryResult){
			
            $result = $this->model->where($cond)->update($data);

            if($result){
                return $this->success('修改成功', "user/goods/index?&vendor_uid={$vendorUid}");
            }else{
                return $this->error('修改失败', "user/vending/index");    
            }
            
        }else{

            //todo 这里要进行一次参数validate

            //这个目前先写死
            $data['remain_count'] = 0;
			$data['status'] = 1;
            $data['uid'] = uniqid();

            $result = $this->model->data($data)->save();
            if($result){
                // return $this->success('修改成功', "user/goods/index");
                return $this->success('修改成功', "user/goods/index?&vendor_uid={$vendorUid}");
            }else{
                return $this->error('修改失败', "user/vending/index");    
            }
        }
    }

	
    public function editInfo(){
        // 先把list拿出来
        $vendorUid = input("param.vendor_uid");

        $this->assign("vendorUid", $vendorUid);
        $uid = input("param.uid");
        $this->assign("uid", $uid);

        $info = false;

        if($uid){
            $cond = "uid='{$uid}'";

            $info = $this->model->where($cond)->find();

            $this->assign("info", $info); 
        }

        $this->assign("info", $info); 

        $list = Db::name("item_type")->select();

        $this->assign("list", $list);

        return $this->view->fetch('edit_goods');

    }

    public function editInfo_ex(){
        // 先把list拿出来
        $vendorUid = input("param.vendor_uid");

        $this->assign("vendorUid", $vendorUid);
        $uid = input("param.uid");
        $this->assign("uid", $uid);

        $info = false;

        if($uid){
            $cond = "uid='{$uid}'";

            $info = $this->model->where($cond)->find();

            $this->assign("info", $info); 
        }

        $this->assign("info", $info); 

        $list = Db::name("item_type")->select();

        $this->assign("list", $list);

        return $this->view->fetch('edit_goods_ex');

    }
	

	
    public function removeInfo(){
			
    	$uid = input('param.uid');
		$vendorInfo = Db::table("think_goods")->where('uid', $uid)->find();
		$vendoruid = $vendorInfo['vendor_uid'];
    	$result = $this->model->where('uid',$uid)->delete();
        trace($vendoruid,"info");
		
    	if(false !== $result){
	    $goodstatus = Db::table("think_vendor")->where('uid',$vendoruid)->find();
		trace($goodstatus,"info");
        $goodstatus = Db::table("think_vendor")->where('uid',$vendoruid)->where('status_vendor',0)->update(['status_vendor' => 1]);      		
    		$data = [
    			'status' => 0,
    			'msg' => '删除成功！',
    		];
    	}else {
    		$data = [
    			'status' => 1,
    			'msg' => '删除失败！',
    		];
    	}
    	echo json_encode($data);
    }
}
