<?php
namespace app\user\controller;
use think\Controller;
// /这里已经在登录的时候记录了cookies了，后面继承base的界面
//都需要request包含账号密码的cookies才能访问。否则跳进去登录界面
class Base extends Controller
{
    public function _initialize()
    {
        //判读会话记录中的用户id是否存在
        //不存在就跳转到登录页面
        if (!session('user_uid')>0){
            //有来路就记录一下，没有就进去后台

            $this->error('登录超时，请重新登录','index/login/index');
        }
    }

    protected function getDataByUid($uid){
        return $this->model->where("uid='{$uid}'")->find();
    }
    
    // //获得省份信息列表
    // public function getProvinceList(){
    //     return $provincelist = Db::name('ybqh')->distinct(true)->field('charProvince')->where("charArea<>'国外'")->order('charProvinceen asc')->select();
    // }
    // //获得某省份的城市信息列表
    // public function getCityList(){
    //     header("Content-Type:text/html; charset=utf-8");
    //     $provinces = input("post.provinces");        
    //     $citylist = Db::name('ybqh')->distinct(true)->field('charCity,charPostCode,charPhoneCode')->where(" intActorid=0 and charProvince='{$provinces}'")->order('charCityen asc')->select();
    //     $data = array('data'=>$citylist);
    //     return $data;
    // }
}
