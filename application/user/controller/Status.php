<?php
namespace app\user\controller;
use app\user\model;
use think\Db;
use think\Controller;
use think\Log;


class Status extends Controller
{	
    public function index($vendoruuid){

		$status_goods = 0;//0代表没有修改
		$status_img = 0;
		$restartStatus =0;
		$status_apk = 0;
		$vendorInfo = Db::table("think_vendor")->where('uuid', $vendoruuid)->find();
		//trace($vendorInfo,"info");
		$vendoruid = $vendorInfo['uid'];
		//trace($vendoruid,"info");
        $goodstatus = Db::table("think_goods")->where('vendor_uid', $vendoruid)->where('status',1)->select();		
        $imgstatus = Db::table("think_img_type")->where('status',1)->select();		
        $itemstatus = Db::table("think_item_type")->where('status',1)->select();
        $vendorstatus = Db::table("think_vendor")->where('uid',$vendoruid)->where('status_vendor',1)->select();
		$apkstatus = Db::table("think_apk")->where('status',1)->select();
 		trace($apkstatus,"info");
		trace($vendorstatus,"aaa");
		//trace($imgstatus,"info");		
		//trace($itemstatus,"info");	   

		if(count($goodstatus)!=0||count($vendorstatus)!=0)
		{
			$status_goods=1;		 
		}
		if(count($imgstatus)!=0)
		{
			$status_img = 1;		 
		}
		if(count($apkstatus)!=0)
		{
			$status_apk = 1;		 
		}		
		if(count($goodstatus)!=0||count($vendorstatus)!=0||count($imgstatus)!=0||count($apkstatus)!=0)
		{
			$restartStatus =1;
		}
		
		
        $staus_all=array("MUID"=>$vendoruuid,'merchandiseStatus'=>$status_goods,"advertisementStatus"=>$status_img,"applicationStatus"=>$status_apk,"restartStatus"=>$restartStatus);
		
        return json_encode($staus_all, JSON_UNESCAPED_SLASHES);
    }
	
	public function resetall($vendoruuid)
	{
		$status_goods = 1;//0代表没有修改
		$status_img = 1;
		$restartStatus =1;
		$status_apk = 1;
		
		$vendorInfo = Db::table("think_vendor")->where('uuid', $vendoruuid)->find();
		$vendoruid = $vendorInfo['uid'];
		
		
        $goodstatus = Db::table("think_goods")->where('vendor_uid', $vendoruid)->where('status',1)->update(['status' => 0]);
        $imgstatus = Db::table("think_img_type")->where('status',1)->update(['status' => 0]);	
        $itemstatus = Db::table("think_item_type")->where('status',1)->update(['status' => 0]);
		$vendorstatus = Db::table("think_vendor")->where('uid',$vendoruid)->where('status_vendor',1)->update(['status_vendor' => 0]);
        $apkstatus = Db::table("think_apk")->where('status',1)->update(['status' => 0]);
		
        $goodstatus = Db::table("think_goods")->where('vendor_uid', $vendoruid)->where('status',1)->select();
        $imgstatus = Db::table("think_img_type")->where('status',1)->select();		
        $itemstatus = Db::table("think_item_type")->where('status',1)->select();
		$vendorstatus = Db::table("think_vendor")->where('uid',$vendoruid)->where('status_vendor',1)->select();
		$apkstatus = Db::table("think_apk")->where('status',1)->select();
		
		trace($goodstatus,"info");
		trace($imgstatus,"info");		
		trace($itemstatus,"info");	 
		if(count($goodstatus)==0&&count($vendorstatus)==0)
		{
			$status_goods  =  0;		 
		}
		if(count($imgstatus)==0)
		{
			$status_img = 0;		 
		}
		if(count($apkstatus)==0)
		{
			$status_apk = 0;		 
		}				
		if(count($goodstatus)==0&&count($vendorstatus)==0&&count($imgstatus)==0&&count($apkstatus)==0)
		{
			$restartStatus =0;
		}
				
		
		

        $staus_all=array("MUID"=>$vendoruuid,'merchandiseStatus'=>$status_goods,"advertisementStatus"=>$status_img,"applicationStatus"=> $status_apk,"restartStatus"=>$restartStatus);
		return json_encode($staus_all, JSON_UNESCAPED_SLASHES);
	}

	

}
